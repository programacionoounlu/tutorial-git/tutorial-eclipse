package calculadora;

import java.util.ArrayList;

public class Calculadora {
	private ArrayList<String> historial;
	
	private void saveToHistory(String operacion) {
		historial.add(operacion);
	}
	public double suma(double a, double b) {
		saveToHistory(a+"+"+b);
		return a + b;
	}
	
	public double resta(double a, double b) {
		saveToHistory(a+"-"+b);
		return a - b;
	}

	public double multiplicacion(double a, double b){
		saveToHistory(a+"*"+b);
		return a * b;
	}

	public double division(double a, double b){
		saveToHistory(a+"/"+b);
		return a / b;
	}

	public double raizCuadrada(double numero) {
		saveToHistory("Raiz cuadrada "+numero);
		return Math.sqrt(numero);
	}

	
	public double potencia(double numero, double potencia) {
		saveToHistory(numero+"^"+potencia);
		return Math.pow(numero, potencia);
	}
}
